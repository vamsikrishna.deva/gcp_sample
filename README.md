# AZ instructions
--

Scenario1 : Developer should be able to deploy from local/IDE to the App Engine environment(Dev/Sandbox)

    TODO: add cloudekode plugin setup, gcloud auth setup...etc
    TODO: add build configs..etc

        1.1 To compile & run in local environments..
        
        ``` mvn install```
        
        ``` mvn spring-boot:run ```   
        
        1.2 To deploy to GCP app engine
        
        ``` mvn appengine:deploy -Dartifact=target/springboot-helloworld-j11-0.0.2-SNAPSHOT.jar  ``` 
        


Scenario2 : Developer should be able to deploy from Gitlab(feature branch) to App Engine Environment(Dev)

    2.1 Commit to feature branch - > build and deploy in dev-test instance, no artifact upload

    2.3 Commit to main branch -> build and upload to artifact with specific version


Scenario3: Developer should be able to deploy to QA/STG/PRD by a specific release version deployed in Dev environments.
    TODO : define the various code scanning,security scanning,..etc

    3.1 Run the pipeline manually , provide below inputs
        (1) target environment
        (2) target version
      Pipeline triggers the infra repo(pulumi) with the details and deploy
  



